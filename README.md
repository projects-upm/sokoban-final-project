# Final Project
## Sokoban
### Rules

- Move the boxes on the goal positions.
- You only can push boxes.
- You can't push two boxes at the same time.
- The level score is the number of boxes  x 100.
- You loose level score when you move the player.
- You add score the level to your total score, when you complete the level.

### Design

For the design we decided to use the MVC, because of its facilities to program and separate the view, the controller and the model in different modules.

#### MVC

![](images/MVC.png)

We define two windows to interact, the Main Menu and the Game View.

##### Main menu

In this window we were create a simple menu to start a new game, load a saved Game or close de application.

![](images/Main-menu.png)

##### Game view

In this window we display information of the current level, the total score of de player and the map to complete.

![](images/Game-view.png)

In the menu bar you can find Game options, Options and actions, and help information.
- Game
    - New Game
    - Load Game
    - Save Game
- Options
    - Restart
    - Undo
    - Redo
- Help
    - Shortcuts
    
#### Game Model Design
GameModel have all the methods to interact with the game.

![](images/GameModel.png)

#### GameManager/GameFactory
- **GameObject:** Base class of the objects in the game.
- **GameObjectManager:** Manage store the objects of the level.
 - **GameObjectFactory:** Create the objects of the level.
 
![](images/GameManager-GameFactory.png)

#### GameObject
![](images/GameObject.png)
#### DrawableObjects/CollisionObjects
![](images/Drawable-Movable.png)
#### LevelLoader/Level/LevelState/SavedLevel
- **LoadManager:** Manage the playable levels.
- **LevelLoader:** Read the level files, and create a playable level.
- **Level:** Store the level information, and all the object and its positions in the game.
- **LevelSave:** Save the current level state and the current progress in a file.
- **SaveLoader:** Restore a previous saved game form a file, the score, all the movements done and the positions of te player and boxes.
- **LevelState:** Store a screenshot of the game, the state of the player and the boxes.

![](images/Level_LevelLoader_LevelState_SavedLevel.png)

#### Level format

First level is the level name.

Second line have the dimension of the level.

Then the maze is draw.
```
Level 1
8 8
++++
+  +
+  +++++
+      +
++W*+# +
+   +  +
+   ++++
+++++
```
#### Saved level format
First line is the id of the level.

Second line have de total score earned.

Then store the level last state and the last movements, first the position of the player and then the position of the boxes in the level. A pair of rows is a movement made.

```
3
368
90-30
30-90 30-70 50-50 50-90 50-70 50-110 30-30 30-110 70-30 30-50 
90-50
50-50 30-30 50-110 70-30 30-90 30-50 30-70 50-70 30-110 50-90 
...
```

