package com.upm.pproject.models;

import com.upm.pproject.models.gameobjects.Directions;
import com.upm.pproject.models.levels.interfaces.ILevel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class GameModelTest {

    @Test
    void TestConstructor() throws IOException {
        GameModel gameModel = new GameModel();
        assertNotNull(gameModel);
    }

    @Test
    void TestConstructorSaveFile() throws IOException {
        GameModel gameModel = new GameModel(new File("saves-test/l1.save"));
        assertNotNull(gameModel);
        assertEquals(1, gameModel.getCurrentLevel().getId());
    }

    @Test
    void TestConstructorException() {
        assertThrows(IOException.class, () -> {
            new GameModel(new File("aaaa"));
        });
    }

    @Nested
    public class GameModelFunctions {

        GameModel gameModel;

        @BeforeEach
        void initTest() throws IOException {
            this.gameModel = new GameModel();
        }

        @Test
        void drawableObjects() {
            assertNotNull(this.gameModel.getCurrentLevel().getDrawableObjects());
        }

        @Test
        void restart() throws IOException {
            ILevel level = this.gameModel.getCurrentLevel();
            this.gameModel.restart();
            assertNotEquals(level, this.gameModel.getCurrentLevel());
        }

        @Test
        void startNextLevel() throws IOException {
            assertTrue(this.gameModel.startNextLevel());
            ILevel level = this.gameModel.getCurrentLevel();
            this.gameModel.startNextLevel();
            assertNotEquals(level.getId(), this.gameModel.getCurrentLevel());
        }

        @Test
        void startNextLevel5Levels() throws IOException {
            this.gameModel.startNextLevel();
            assertEquals(2, this.gameModel.getCurrentLevel().getId());
            this.gameModel.startNextLevel();
            assertEquals(3, this.gameModel.getCurrentLevel().getId());
            this.gameModel.startNextLevel();
            assertFalse(this.gameModel.startNextLevel());
        }

        @Test
        void startNewGame() throws IOException {
            this.gameModel.startNextLevel();
            this.gameModel.startNewGame();
            assertEquals(1, this.gameModel.getCurrentLevel().getId());
        }

        @Test
        void undo() {
            this.gameModel.move(Directions.UP);
            assertEquals(99, this.gameModel.getLevelScore());
            this.gameModel.undo();
            assertEquals(100, this.gameModel.getLevelScore());
        }

        @Test
        void undoEmpty() {
            this.gameModel.undo();
            assertEquals(100, this.gameModel.getLevelScore());
        }

        @Test
        void redo() {
            this.gameModel.move(Directions.UP);
            assertEquals(99, this.gameModel.getLevelScore());
            this.gameModel.undo();
            assertEquals(100, this.gameModel.getLevelScore());
            this.gameModel.redo();
            assertEquals(99, this.gameModel.getLevelScore());
        }

        @Test
        void redoEmpty() {
            this.gameModel.redo();
            assertEquals(100, this.gameModel.getLevelScore());
            this.gameModel.move(Directions.UP);
            this.gameModel.undo();
            this.gameModel.move(Directions.DOWN);
            this.gameModel.redo();
            assertEquals(99, this.gameModel.getLevelScore());
        }

        @Test
        void saveGame() throws IOException {
            this.gameModel.saveGame(new File("saves-test/test.save"));
            assertEquals(1, this.gameModel.getCurrentLevel().getId());
        }

        @Test
        void saveGameMovements() throws IOException {
            this.gameModel.move(Directions.UP);
            this.gameModel.saveGame(new File("saves-test/test.save"));
            assertEquals(1, this.gameModel.getCurrentLevel().getId());
        }

        @Test
        void saveGameException() {
            assertThrows(NullPointerException.class, () -> {
                this.gameModel.saveGame(null);
            });
        }

        @Test
        void move() {
            this.gameModel.move(Directions.DOWN);
            this.gameModel.move(Directions.UP);
            this.gameModel.move(Directions.RIGHT);
            this.gameModel.move(Directions.LEFT);
            assertEquals(96, this.gameModel.getLevelScore());
        }

        @Test
        void moveUndo() {
            this.gameModel.move(Directions.DOWN);
            this.gameModel.move(Directions.UP);
            this.gameModel.undo();
            this.gameModel.move(Directions.RIGHT);
            this.gameModel.move(Directions.LEFT);
            assertEquals(97, this.gameModel.getLevelScore());
        }

        @Test
        void moveWallCollision() {
            this.gameModel.move(Directions.LEFT);
            assertEquals(100, this.gameModel.getLevelScore());
        }

        @Test
        void moveBoxCollision() {
            this.gameModel.move(Directions.UP);
            this.gameModel.move(Directions.RIGHT);
            this.gameModel.move(Directions.RIGHT);
            this.gameModel.move(Directions.RIGHT);
            this.gameModel.move(Directions.DOWN);
            assertEquals(95, this.gameModel.getLevelScore());
            this.gameModel.move(Directions.DOWN);
            assertEquals(95, this.gameModel.getLevelScore());
        }

        @Test
        void getCurrentLevel() throws IOException {
            assertNotNull(this.gameModel.getCurrentLevel());
            this.gameModel.startNewGame();
            assertNotNull(this.gameModel.getCurrentLevel());
            this.gameModel.startNextLevel();
            assertNotNull(this.gameModel.getCurrentLevel());
        }

        @Test
        void getLevelName() throws IOException {
            assertEquals("Level 1", this.gameModel.getLevelName());
            this.gameModel.startNextLevel();
            assertEquals("Level 2", this.gameModel.getLevelName());
            this.gameModel.startNewGame();
            assertEquals("Level 1", this.gameModel.getLevelName());
        }

        @Test
        void getTotalScore() throws IOException {
            assertEquals(0, this.gameModel.getTotalScore());
            this.gameModel.startNextLevel();
            assertEquals(100, this.gameModel.getTotalScore());
            this.gameModel.startNewGame();
            assertEquals(0, this.gameModel.getTotalScore());
        }

        @Test
        void getLevelScore() throws IOException {
            assertEquals(100, this.gameModel.getLevelScore());
            this.gameModel.startNextLevel();
            assertEquals(600, this.gameModel.getLevelScore());
            this.gameModel.startNewGame();
            assertEquals(100, this.gameModel.getLevelScore());
        }


        @Nested
        public class Completion2 {

            @BeforeEach
            void initTest() throws IOException {
                gameModel = new GameModel(new File("saves-test/testCompletion2.save"));
            }

            @Test
            void testNotCompletionDiagonal() {
                assertFalse(gameModel.checkCompletion());
            }
        }

        @Nested
        public class Completion {

            @BeforeEach
            void initTest() throws IOException {
                gameModel = new GameModel(new File("saves-test/testCompletion.save"));
            }

            @Test
            void testCompletion() {
                gameModel.move(Directions.UP);
                assertTrue(gameModel.checkCompletion());
            }

            @Test
            void testNotCompletion() {
                gameModel.move(Directions.LEFT);
                assertFalse(gameModel.checkCompletion());
            }

        }

        @Nested
        public class CollisionBoxBox {

            @BeforeEach
            void initTest() throws IOException {
                gameModel = new GameModel(new File("saves-test/BoxBoxCollision.save"));
            }

            @Test
            void boxBoxCollision() {
                gameModel.move(Directions.LEFT);
                assertEquals(591, gameModel.getLevelScore());
            }
        }
    }
}