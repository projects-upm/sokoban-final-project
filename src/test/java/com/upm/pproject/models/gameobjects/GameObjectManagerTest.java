package com.upm.pproject.models.gameobjects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class GameObjectManagerTest {

    GameObjectManager gameObjectManager;

    @BeforeEach
    void initTest() {
        this.gameObjectManager = new GameObjectManager();
    }

    @Test
    void getPlayer() {
        this.gameObjectManager.newGameObject(0,0, GameObjectType.PLAYER);
        assertNotNull(this.gameObjectManager.getPlayer());
    }

    @Test
    void setPlayer() {
        Player player = new Player(0,0);
        this.gameObjectManager.setPlayer(player);
        assertEquals(player.getX(), this.gameObjectManager.getPlayer().getX());
        assertEquals(player.getY(), this.gameObjectManager.getPlayer().getY());
    }

    @Test
    void getCollisionPlayer() {
        Player player = new Player(0,0);
        this.gameObjectManager.setPlayer(player);
        assertEquals(player.getX(), this.gameObjectManager.getCollisionPlayer().getGameObject().getX());
        assertEquals(player.getY(), this.gameObjectManager.getCollisionPlayer().getGameObject().getY());
    }

    @Test
    void getCollisionBoxes() {
        this.gameObjectManager.newGameObject(0,0,GameObjectType.BOX);
        this.gameObjectManager.newGameObject(0,0,GameObjectType.BOX);
        assertEquals(2, this.gameObjectManager.getCollisionBoxes().size());
    }

    @Test
    void getCollisionWalls() {
        this.gameObjectManager.newGameObject(0,0,GameObjectType.WALL);
        this.gameObjectManager.newGameObject(0,0,GameObjectType.WALL);
        assertEquals(2, this.gameObjectManager.getCollisionWalls().size());
    }

    @Test
    void getWalls() {
        this.gameObjectManager.newGameObject(0,0,GameObjectType.WALL);
        this.gameObjectManager.newGameObject(0,0,GameObjectType.WALL);
        assertEquals(2, this.gameObjectManager.getWalls().size());
    }

    @Test
    void getGoals() {
        this.gameObjectManager.newGameObject(0,0,GameObjectType.GOAL);
        this.gameObjectManager.newGameObject(0,0,GameObjectType.GOAL);
        assertEquals(2, this.gameObjectManager.getGoals().size());
    }

    @Test
    void getBoxes() {
        this.gameObjectManager.newGameObject(0,0,GameObjectType.BOX);
        this.gameObjectManager.newGameObject(0,0,GameObjectType.BOX);
        assertEquals(2, this.gameObjectManager.getBoxes().size());
    }

    @Test
    void setBoxes() {
        Set<Box> boxes = new HashSet<>();
        boxes.add(new Box(0,0));
        boxes.add(new Box(0,1));
        boxes.add(new Box(1,0));
        boxes.add(new Box(1,1));
        this.gameObjectManager.setBoxes(boxes);
        assertEquals(4, this.gameObjectManager.getBoxes().size());
    }

    @Test
    void newGameObjectBox() {
        this.gameObjectManager.newGameObject(0,0,GameObjectType.BOX);
        assertEquals(1, this.gameObjectManager.getBoxes().size());
    }

    @Test
    void newGameObjectPlayer() {
        this.gameObjectManager.newGameObject(0,0,GameObjectType.PLAYER);
        assertEquals(0, this.gameObjectManager.getPlayer().getX());
        assertEquals(0, this.gameObjectManager.getPlayer().getY());
    }

    @Test
    void newGameObjectGoal() {
        this.gameObjectManager.newGameObject(0,0,GameObjectType.GOAL);
        assertEquals(1, this.gameObjectManager.getGoals().size());
    }

    @Test
    void newGameObjectWall() {
        this.gameObjectManager.newGameObject(0,0,GameObjectType.WALL);
        assertEquals(1, this.gameObjectManager.getWalls().size());
    }

    @Test
    void getDrawableObjects() {
        this.gameObjectManager.newGameObject(0,0,GameObjectType.BOX);
        this.gameObjectManager.newGameObject(0,0,GameObjectType.PLAYER);
        this.gameObjectManager.newGameObject(0,0,GameObjectType.GOAL);
        this.gameObjectManager.newGameObject(0,0,GameObjectType.WALL);
        assertEquals(4, this.gameObjectManager.getDrawableObjects().size());
    }
}