package com.upm.pproject.models.levels;

import com.upm.pproject.models.levels.interfaces.ILevel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LevelTest {

    ILevel level;

    @BeforeEach
    void initTest() {
        this.level = new Level(1, "Level 1", 10, 10);
    }

    @Test
    void getGameObjectManager() {
        assertNotNull(this.level.getGameObjectManager());
    }

    @Test
    void getDrawableObjects() {
        assertNotNull(this.level.getDrawableObjects());
    }

    @Test
    void getDimX() {
        assertEquals(10, this.level.getDimX());
    }

    @Test
    void getDimY() {
        assertEquals(10, this.level.getDimY());
    }

    @Test
    void getName() {
        assertEquals("Level 1", this.level.getName());
    }

    @Test
    void getId() {
        assertEquals(1, this.level.getId());
    }
}