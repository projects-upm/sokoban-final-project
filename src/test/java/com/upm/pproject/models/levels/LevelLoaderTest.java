package com.upm.pproject.models.levels;

import com.upm.pproject.models.levels.interfaces.ILevel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class LevelLoaderTest {

    LevelManager levelLoader;
    ILevel level;

    @BeforeEach
    void initTest() throws IOException {
        levelLoader = new LevelManager();
        this.level = this.levelLoader.loadLevel(1);
    }

    @Test
    void initLevels() throws IOException {
        assertDoesNotThrow(() -> this.levelLoader.initLevels("levels/"));
        assertThrows(NullPointerException.class, () -> this.levelLoader.initLevels(null));

    }

    @Test
    void saveLevelProgress1() {
        assertThrows(NullPointerException.class, () -> this.levelLoader.saveLevelProgress(level, 28, new LinkedList<>(), null));
        assertThrows(FileNotFoundException.class, () -> this.levelLoader.saveLevelProgress(level, 28, new LinkedList<>(), new File("saves-test/")));
    }

    @Test
    void saveLevelProgress2() {
        assertDoesNotThrow(() -> this.levelLoader.saveLevelProgress(level, 28, new LinkedList<>(), new File("saves-test/tttsss.save")));
        assertThrows(FileNotFoundException.class, () -> this.levelLoader.saveLevelProgress(level, 28, new LinkedList<>(), new File("")));
    }

    @Test
    void loadSaveLevel() {
        assertDoesNotThrow(() -> this.levelLoader.loadSaveLevel(new File("saves-test/l1.save")));
    }

    @Test
    void loadSaveLevelNull() {
        assertThrows(NullPointerException.class, () -> this.levelLoader.loadSaveLevel(null));
    }

    @Test
    void loadSaveLevelFileNotExist() {
        assertThrows(IOException.class, () -> this.levelLoader.loadSaveLevel(new File("")));
        assertThrows(FileNotFoundException.class, () -> this.levelLoader.loadSaveLevel(new File("aaaaaa")));
    }

    @Test
    void loadSaveLevelIsDirectory() {
        assertThrows(IOException.class, () -> this.levelLoader.loadSaveLevel(new File("saves-test/")));
    }

    @Test
    void loadSaveLevelWrongID() {
        assertThrows(NumberFormatException.class, () -> this.levelLoader.loadSaveLevel(new File("saves-test/WrongID.save")));
    }

    @Test
    void loadSaveLevelWrongScore() {
        assertThrows(NumberFormatException.class, () -> this.levelLoader.loadSaveLevel(new File("saves-test/WrongScore.save")));
    }

    @Test
    void loadSaveLevelWrongBox() {
        assertThrows(IOException.class, () -> this.levelLoader.loadSaveLevel(new File("saves-test/WrongBox.save")));
    }

    @Test
    void loadSaveLevelWrongBoxFormat() {
        assertThrows(IOException.class, () -> this.levelLoader.loadSaveLevel(new File("saves-test/WrongBoxFormat.save")));
    }

    @Test
    void loadSaveLevelWrongNoBox() {
        assertThrows(NullPointerException.class, () -> this.levelLoader.loadSaveLevel(new File("saves-test/WrongNoBox.save")));
    }

    @Test
    void loadSaveLevelWrongPlayer() {
        assertThrows(IOException.class, () -> this.levelLoader.loadSaveLevel(new File("saves-test/WrongPlayer.save")));
    }

    @Test
    void loadSaveLevelEmptyMoves() {
        assertDoesNotThrow(() -> this.levelLoader.loadSaveLevel(new File("saves-test/l2.save")));
    }
}