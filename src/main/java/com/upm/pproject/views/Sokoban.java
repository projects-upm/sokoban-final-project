package com.upm.pproject.views;

import com.upm.pproject.controllers.MainMenuController;
import com.upm.pproject.models.MainMenuModel;

import java.awt.*;


public class Sokoban {
    /**
     * Main class
     * @param args
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            MainMenuModel model = new MainMenuModel();
            MainMenuController controller = new MainMenuController(null, model);
            MainMenuView view = new MainMenuView(controller);
            controller.setView(view);
        });
    }
}

