package com.upm.pproject.views;

import com.upm.pproject.models.GameModel;
import com.upm.pproject.views.interfaces.IView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class SaveListener implements ActionListener {

    private static final Logger logger = LogManager.getLogger(SaveListener.class);

    private IView frame;

    /**
     * Default constructor
     * @param frame
     */
    SaveListener(IView frame) {
        this.frame = frame;
    }

    /**
     * Listener to saves files
     * Create directory of saves if not exist
     * @param actionEvent
     */
    @Override
    public void actionPerformed(ActionEvent actionEvent) {

        File directory = new File("./saves");
        if (!directory.exists()) {
            directory.mkdir();
        }
        JFileChooser c = new JFileChooser("./saves");
        c.addChoosableFileFilter(new FileNameExtensionFilter("SAVE GAME", "save"));
        // Demonstrate "Open" dialog:
        int rVal = c.showSaveDialog((JFrame) frame.getController().getView());
        if (rVal == JFileChooser.APPROVE_OPTION) {

            // get the full path of the file
            String absolutePath = c.getSelectedFile().getAbsolutePath();

            // does the selected file have an extension of docx?
            // if yes then exclude the extension, if no, then add .docx to the file name
            if (!absolutePath.substring(absolutePath.lastIndexOf('.') + 1).equals("save"))
                absolutePath += ".save";

            try {
                String s = "Try save as file: " + absolutePath;
                logger.debug(s);
                File fileWriter = new File(absolutePath);
                if (!fileWriter.exists()) {
                    if (fileWriter.createNewFile()) {
                        GameModel model = (GameModel) frame.getController().getModel();
                        model.saveGame(fileWriter);
                    } else {
                        logger.error("Impossible to create save file.");
                    }
                } else {
                    GameModel model = (GameModel) frame.getController().getModel();
                    model.saveGame(fileWriter);
                }
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
    }
}
