package com.upm.pproject.views.interfaces;


import com.upm.pproject.controllers.interfaces.IController;

public interface IView {
    IController getController();

    void setController(IController gameController);
}
