package com.upm.pproject.views;

import com.upm.pproject.controllers.GameController;
import com.upm.pproject.controllers.MainMenuController;
import com.upm.pproject.controllers.interfaces.IController;
import com.upm.pproject.models.GameModel;
import com.upm.pproject.views.interfaces.IView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.io.IOException;

public class MainMenuView extends JFrame implements IView {

    private static final Logger logger = LogManager.getLogger(MainMenuView.class);

    private MainMenuController mainMenuController;

    /**
     * Creates menu view
     * @param controller
     */
    public MainMenuView(IController controller) {
        this.mainMenuController = (MainMenuController) controller;

        setTitle("Sokoban");//Title
        setLayout(new BorderLayout());
        createFields();
        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    /**
     * Create fields of the view
     */
    private void createFields() {
        JPanel menu = new JPanel(new GridBagLayout());
        menu.setBorder(new EmptyBorder(50, 50, 50, 50));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.fill = GridBagConstraints.HORIZONTAL;

        JButton newGame = new JButton("New Game");
        newGame.addActionListener(e -> {
            GameModel model = null;
            GameController gameController = new GameController(null, null);
            try {
                model = new GameModel();
            } catch (IOException ex) {
                logger.error(ex.getMessage());
            }
            GameView view = new GameView(gameController);
            gameController.setView(view);
            gameController.setModel(model);
            this.dispose();
        });
        menu.add(newGame, gbc); //New game

        JButton loadGame = new JButton("Load Game");
        loadGame.addActionListener(new OpenListener(MainMenuView.this));
        menu.add(loadGame, gbc); //Load game

        JButton exit = new JButton("Exit");
        exit.addActionListener(e -> this.dispose());
        menu.add(exit, gbc); //Exit

        add(menu, BorderLayout.CENTER);
    }

    @Override
    public IController getController() {
        return mainMenuController;
    }

    @Override
    public void setController(IController gameController) {
        this.mainMenuController = (MainMenuController) gameController;
    }
}
