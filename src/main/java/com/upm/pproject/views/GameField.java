package com.upm.pproject.views;

import com.upm.pproject.controllers.interfaces.IEventListener;
import com.upm.pproject.models.gameobjects.Directions;
import com.upm.pproject.models.gameobjects.drawable.interfaces.IDrawableObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class GameField extends JPanel {

    private static final Logger logger = LogManager.getLogger(GameField.class);

    private final GameView view;

    /**
     * Constructor to create game field
     * @param view
     * @param eventListener keys listener
     */
    public GameField(GameView view, IEventListener eventListener) {
        this.view = view;
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                int keyCode = e.getKeyCode();
                if (!e.isControlDown()) {
                    if (keyCode == KeyEvent.VK_LEFT) {
                        logger.debug("LEFT");
                        eventListener.move(Directions.LEFT);
                    } else if (keyCode == KeyEvent.VK_RIGHT) {
                        logger.debug("RIGHT");
                        eventListener.move(Directions.RIGHT);
                    } else if (keyCode == KeyEvent.VK_UP) {
                        logger.debug("UP");
                        eventListener.move(Directions.UP);
                    } else if (keyCode == KeyEvent.VK_DOWN) {
                        logger.debug("DOWN");
                        eventListener.move(Directions.DOWN);
                    } else if (keyCode == KeyEvent.VK_R) {
                        logger.debug("R");
                        eventListener.restart();
                    }
                } else {
                    if (keyCode == KeyEvent.VK_U) {
                        logger.debug("UNDO");
                        eventListener.undo();
                    } else if (keyCode == KeyEvent.VK_R) {
                        logger.debug("REDO");
                        eventListener.redo();
                    }
                }
            }
        });
        setFocusable(true);
    }

    /**
     * Draw field
     * @param g
     */
    @Override
    public void paint(Graphics g) {
        g.setColor(Color.BLACK);//Field color
        g.fillRect(0, 0, view.getWidth(), view.getHeight());//Fills the rectangle

        g.setColor(Color.WHITE);
        g.drawString(view.getLevelName(), 20, 20);//Level name
        g.drawString("Score: " + view.getTotalScore(), 20, 35);//Total score
        g.drawString("Level Score: " + view.getLevelScore(), 20, 50);//Level score

        int xOffset = 30;
        int yOffset = 65;

        for (IDrawableObject drawableObject : view.getDrawableObjects()) {
            drawableObject.draw(g, xOffset, yOffset);
        }
    }
}
