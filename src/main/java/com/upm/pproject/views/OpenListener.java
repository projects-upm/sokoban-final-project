package com.upm.pproject.views;

import com.upm.pproject.controllers.GameController;
import com.upm.pproject.models.GameModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class OpenListener implements ActionListener {

    private static final Logger logger = LogManager.getLogger(OpenListener.class);

    private JFrame frame;

    /**
     * Default Constructor
     * Creates frame
     * @param frame
     */
    OpenListener(JFrame frame) {
        this.frame = frame;
    }

    /**
     * Listener to load saves
     * Create directory of saves if not exist
     * @param actionEvent
     */
    @Override
    public void actionPerformed(ActionEvent actionEvent) {

        File directory = new File("./saves");
        if (!directory.exists()) {
            directory.mkdir();
        }
        JFileChooser c = new JFileChooser("./saves");
        FileNameExtensionFilter filter = new FileNameExtensionFilter("SAVE GAME", "save", "save");
        c.setFileFilter(filter);
        // Demonstrate "Open" dialog:
        int rVal = c.showOpenDialog(frame);
        if (rVal == JFileChooser.APPROVE_OPTION) {
            logger.debug(c.getSelectedFile().getAbsolutePath());
            logger.debug(c.getCurrentDirectory().toString());
            GameModel model = null;
            GameController gameController = new GameController(null, null);
            try {
                model = new GameModel(c.getSelectedFile().getAbsoluteFile());
            } catch (IOException ex) {
                logger.error(ex.getMessage());
            }
            GameView view = new GameView(gameController);
            gameController.setView(view);
            gameController.setModel(model);
            this.frame.dispose();
        }
    }
}
