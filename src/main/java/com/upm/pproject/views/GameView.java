package com.upm.pproject.views;

import com.upm.pproject.controllers.GameController;
import com.upm.pproject.controllers.interfaces.IController;
import com.upm.pproject.controllers.interfaces.IEventListener;
import com.upm.pproject.models.gameobjects.drawable.interfaces.IDrawableObject;
import com.upm.pproject.views.interfaces.IView;

import javax.swing.*;
import java.util.Set;

public class GameView extends JFrame implements IView {

    private GameController gameController;
    private GameField field;

    /**
     * Constructor to create the view of the game
     * @param controller
     */
    public GameView(IController controller) {
        this.gameController = (GameController) controller;
        this.field = new GameField(this, (IEventListener) controller);
        add(this.field);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(500, 500);//Size
        setLocationRelativeTo(null);
        setTitle("Sokoban");//Title

        createMenu();//Creates the menu

        setVisible(true);
    }

    /**
     * Creates menu in the game with diferents options
     */
    private void createMenu() {
        JMenuBar mb = new JMenuBar();
        setJMenuBar(mb);

        JMenu game = new JMenu("Game");
        mb.add(game);

        JMenuItem newGame = new JMenuItem("New game");
        newGame.addActionListener(e -> this.gameController.startNewGame());
        game.add(newGame);

        JMenuItem loadGame = new JMenuItem("Load game");
        loadGame.addActionListener(new OpenListener(GameView.this));
        game.add(loadGame);

        JMenuItem saveGame = new JMenuItem("Save game");
        saveGame.addActionListener(new SaveListener(GameView.this));
        game.add(saveGame);

        JMenu options = new JMenu("Options");
        mb.add(options);

        JMenuItem restart = new JMenuItem("Restart level");
        restart.addActionListener(e -> this.gameController.restart());
        options.add(restart);

        JMenuItem undo = new JMenuItem("Undo step");
        undo.addActionListener(e -> this.gameController.undo());
        options.add(undo);

        JMenuItem redo = new JMenuItem("Redo step");
        redo.addActionListener(e -> this.gameController.redo());
        options.add(redo);

        JMenu help = new JMenu("Help");
        mb.add(help);

        JMenuItem keyboardActions = new JMenuItem("Shortcuts");
        keyboardActions.addActionListener(e -> this.keyboardActionsDialog());
        help.add(keyboardActions);
    }

    @Override
    public IController getController() {
        return this.gameController;
    }

    @Override
    public void setController(IController gameController) {
        this.gameController = (GameController) gameController;
    }

    public Set<IDrawableObject> getDrawableObjects() {
        return gameController.getDrawableObjects();
    }

    public String getLevelName() {
        return gameController.getLevelName();
    }

    public int getTotalScore() {
        return gameController.getTotalScore();
    }

    public int getLevelScore() {
        return gameController.getLevelScore();
    }

    public void update() {
        this.field.repaint();
    }

    /**
     * Level completed message
     * @param level
     */
    public void completed(int level) {
        JOptionPane.showMessageDialog(
                this,
                "You have completed the level " + level + ", congratulations!",
                "Level completed",
                JOptionPane.INFORMATION_MESSAGE);
        this.gameController.startNextLevel();
    }

    /**
     * Game completed when no are more levels
     */
    public void gameCompleted() {
        JOptionPane.showMessageDialog(
                this,
                "You have finished the game, congratulations!",
                "Game completed",
                JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Keyboard help
     */
    private void keyboardActionsDialog() {
        JOptionPane.showMessageDialog(
                this,
                "R - Restart\n" +
                        "\u2191 - Up\n" +
                        "\u2193 - Down\n" +
                        "\u2190 - Left\n" +
                        "\u2192 - Right\n" +
                        "CTRL+U - Undo Move\n" +
                        "CTRL+R - Redo Move",
                "Keyboard Shortcuts",
                JOptionPane.INFORMATION_MESSAGE);
    }
}
