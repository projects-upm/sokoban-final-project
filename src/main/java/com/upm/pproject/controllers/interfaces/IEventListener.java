package com.upm.pproject.controllers.interfaces;

import com.upm.pproject.models.gameobjects.Directions;


public interface IEventListener {

    void undo();

    void redo();

    void move(Directions direction);

    void restart();

    void startNextLevel();

    void levelCompleted(int level);

    void startNewGame();
}
