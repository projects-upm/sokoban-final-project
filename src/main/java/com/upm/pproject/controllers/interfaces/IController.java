package com.upm.pproject.controllers.interfaces;

import com.upm.pproject.models.interfaces.IModel;
import com.upm.pproject.views.interfaces.IView;

public interface IController {
    IModel getModel();

    void setModel(IModel model);

    IView getView();

    void setView(IView view);
}
