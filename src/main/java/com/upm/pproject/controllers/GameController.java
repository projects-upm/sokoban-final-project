package com.upm.pproject.controllers;

import com.upm.pproject.controllers.interfaces.IController;
import com.upm.pproject.controllers.interfaces.IEventListener;
import com.upm.pproject.models.GameModel;
import com.upm.pproject.models.MainMenuModel;
import com.upm.pproject.models.gameobjects.Directions;
import com.upm.pproject.models.gameobjects.drawable.interfaces.IDrawableObject;
import com.upm.pproject.models.interfaces.IModel;
import com.upm.pproject.views.GameView;
import com.upm.pproject.views.MainMenuView;
import com.upm.pproject.views.interfaces.IView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Set;

public class GameController implements IController, IEventListener {

    private static final Logger logger = LogManager.getLogger(GameController.class);

    private GameView view;
    private GameModel model;

    /**
     * Constructor
     * @param view
     * @param model
     */
    public GameController(IView view, IModel model) {
        this.view = (GameView) view;
        this.model = (GameModel) model;
    }

    @Override
    public IModel getModel() {
        return this.model;
    }

    @Override
    public void setModel(IModel model) {
        this.model = (GameModel) model;
    }

    @Override
    public IView getView() {
        return this.view;
    }

    @Override
    public void setView(IView view) {
        this.view = (GameView) view;
    }

    public Set<IDrawableObject> getDrawableObjects() {
        return model.getCurrentLevel().getDrawableObjects();
    }

    public String getLevelName() {
        return model.getLevelName();
    }

    public int getTotalScore() {
        return model.getTotalScore();
    }

    public int getLevelScore() {
        return model.getLevelScore();
    }

    /**
     * Undo the movement
     */
    @Override
    public void undo() {
        this.model.undo();
        this.view.update();
    }

    /**
     * Remake the movement
     */
    @Override
    public void redo() {
        this.model.redo();
        this.view.update();
    }

    /**
     * Move in the desired direction
     * @param direction
     */
    @Override
    public void move(Directions direction) {
        this.model.move(direction);
        this.view.update();
        if (this.model.checkCompletion()) {
            this.levelCompleted(this.model.getCurrentLevel().getId());
        }
    }

    /**
     * Start new game with init level 1 and score 0
     */
    @Override
    public void startNewGame() {
        try {
            this.model.startNewGame();
        } catch (IOException ex) {
            logger.error(ex);
        }
        this.view.update();
    }

    /**
     * Restart level
     */
    @Override
    public void restart() {
        try {
            this.model.restart();
        } catch (IOException ex) {
            logger.error(ex);
        }
        this.view.update();
    }

    /**
     * If level completed start next level
     * Else update view and show message of game completed
     */
    @Override
    public void startNextLevel() {
        try {
            if (this.model.startNextLevel()) {
                this.view.update();
            } else {
                this.view.gameCompleted();
                MainMenuModel mainMenuModel = new MainMenuModel();
                MainMenuController controller = new MainMenuController(null, mainMenuModel);
                MainMenuView mainMenuView = new MainMenuView(controller);
                controller.setView(mainMenuView);
                this.view.dispose();
            }
        } catch (IOException ex) {
            logger.error(ex);
        }
    }

    /**
     * Checks if the level is completed and update the view
     * @param level
     */
    @Override
    public void levelCompleted(int level) {
        view.update();
        view.completed(level);
    }
}
