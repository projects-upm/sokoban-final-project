package com.upm.pproject.controllers;

import com.upm.pproject.controllers.interfaces.IController;
import com.upm.pproject.models.MainMenuModel;
import com.upm.pproject.models.interfaces.IModel;
import com.upm.pproject.views.MainMenuView;
import com.upm.pproject.views.interfaces.IView;

public class MainMenuController implements IController {
    private MainMenuView view;
    private MainMenuModel model;

    /**
     * Constructor
     * @param view
     * @param model
     */
    public MainMenuController(IView view, IModel model) {
        this.view = (MainMenuView) view;
        this.model = (MainMenuModel) model;
    }

    @Override
    public IModel getModel() {
        return this.model;
    }

    @Override
    public void setModel(IModel model) {
        this.model = (MainMenuModel) model;
    }

    @Override
    public IView getView() {
        return this.view;
    }

    @Override
    public void setView(IView view) {
        this.view = (MainMenuView) view;
    }
}
