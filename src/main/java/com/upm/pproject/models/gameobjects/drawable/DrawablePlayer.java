package com.upm.pproject.models.gameobjects.drawable;

import com.upm.pproject.models.gameobjects.Player;
import com.upm.pproject.models.gameobjects.drawable.interfaces.IDrawableObject;

import java.awt.*;

public class DrawablePlayer implements IDrawableObject {

    private Player player;

    /**
     * Constructor to set player
     * @param player
     */
    public DrawablePlayer(Player player) {
        this.player = player;
    }

    /**
     * Draw player
     * @param graphics
     */
    @Override
    public void draw(Graphics graphics) {
        graphics.setColor(Color.ORANGE);//Player color

        int leftUpperCornerX = player.getX() - player.getWidth() / 2;
        int leftUpperCornerY = player.getY() - player.getHeight() / 2;

        graphics.drawOval(leftUpperCornerX, leftUpperCornerY, player.getWidth(), player.getHeight());//Draw outline
        graphics.fillOval(leftUpperCornerX, leftUpperCornerY, player.getWidth(), player.getHeight());//Fill oval
    }

    /**
     * Draw offsets
     * @param graphics
     * @param xOffset
     * @param yOffset
     */
    @Override
    public void draw(Graphics graphics, int xOffset, int yOffset) {
        graphics.setColor(Color.ORANGE);//Player color

        int leftUpperCornerX = xOffset + (player.getX() - player.getWidth() / 2);
        int leftUpperCornerY = yOffset + (player.getY() - player.getHeight() / 2);

        graphics.drawOval(leftUpperCornerX, leftUpperCornerY, player.getWidth(), player.getHeight());//Draw outline
        graphics.fillOval(leftUpperCornerX, leftUpperCornerY, player.getWidth(), player.getHeight());//Fill oval
    }
}
