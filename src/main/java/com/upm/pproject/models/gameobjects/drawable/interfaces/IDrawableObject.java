package com.upm.pproject.models.gameobjects.drawable.interfaces;

import java.awt.*;

public interface IDrawableObject {
    void draw(Graphics graphics);

    void draw(Graphics graphics, int xOffset, int yOffset);
}
