package com.upm.pproject.models.gameobjects;

import com.upm.pproject.models.GameModel;
import com.upm.pproject.models.gameobjects.interfaces.IObject;

public abstract class GameObject implements IObject {

    protected int x;
    protected int y;
    protected int width;
    protected int height;

    /**
     * Constructor to set x and y
     * @param x
     * @param y
     */
    public GameObject(int x, int y) {
        this.x = x;
        this.y = y;
        this.width = this.height = GameModel.FIELD_SELL_SIZE;
    }

    /**
     * Constructor to set x, y, width and height
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public GameObject(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    @Override
    public int getX() {
        return this.x;
    }

    @Override
    public int getY() {
        return this.y;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public void setPos(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
