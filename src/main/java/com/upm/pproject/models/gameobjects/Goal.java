package com.upm.pproject.models.gameobjects;

public class Goal extends GameObject {

    private static final int DEFAULT_SIZE = 2;

    /**
     * Dimensional Goal
     * @param x
     * @param y
     */
    public Goal(int x, int y) {
        super(x, y, DEFAULT_SIZE, DEFAULT_SIZE);
    }
}
