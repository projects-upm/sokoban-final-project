package com.upm.pproject.models.gameobjects;

public class Box extends GameObject {
    /**
     * Dimensional box
     * @param x
     * @param y
     */
    public Box(int x, int y) {
        super(x, y);
    }
}
