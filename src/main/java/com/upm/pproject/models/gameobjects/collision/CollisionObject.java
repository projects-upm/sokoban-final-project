package com.upm.pproject.models.gameobjects.collision;

import com.upm.pproject.models.GameModel;
import com.upm.pproject.models.gameobjects.Directions;
import com.upm.pproject.models.gameobjects.GameObject;
import com.upm.pproject.models.gameobjects.collision.interfaces.ICollisionObject;

public abstract class CollisionObject implements ICollisionObject {

    protected GameObject gameObject;

    @Override
    public GameObject getGameObject() {
        return this.gameObject;
    }

    /**
     * Check if a object collide
     * @param collisionObject
     * @param direction
     * @return true or false
     */
    @Override
    public boolean isCollision(ICollisionObject collisionObject, Directions direction) {
        int xAfterMove = gameObject.getX();
        int yAfterMove = gameObject.getY();

        if (direction == Directions.LEFT) {
            xAfterMove -= GameModel.FIELD_SELL_SIZE;
        } else if (direction == Directions.RIGHT) {
            xAfterMove += GameModel.FIELD_SELL_SIZE;
        } else if (direction == Directions.UP) {
            yAfterMove -= GameModel.FIELD_SELL_SIZE;
        } else {
            yAfterMove += GameModel.FIELD_SELL_SIZE;
        }

        return xAfterMove == collisionObject.getGameObject().getX() && yAfterMove == collisionObject.getGameObject().getY();
    }

}
