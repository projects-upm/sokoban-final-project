package com.upm.pproject.models.gameobjects;

public enum GameObjectType {
    PLAYER, WALL, GOAL, BOX
}
