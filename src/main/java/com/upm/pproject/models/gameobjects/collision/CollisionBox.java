package com.upm.pproject.models.gameobjects.collision;


import com.upm.pproject.models.gameobjects.Box;
import com.upm.pproject.models.gameobjects.collision.interfaces.IMovableObject;


public class CollisionBox extends CollisionObject implements IMovableObject {
    /**
     * Constructor to set box
     * @param box
     */
    public CollisionBox(Box box) {
        this.gameObject = box;
    }

    /**
     * Move in the desired direction
     * @param deltaX xpos
     * @param deltaY ypos
     */
    @Override
    public void move(int deltaX, int deltaY) {
        gameObject.setPos(gameObject.getX() + deltaX, gameObject.getY() + deltaY);
    }

}
