package com.upm.pproject.models.gameobjects.interfaces;

import com.upm.pproject.models.gameobjects.*;
import com.upm.pproject.models.gameobjects.collision.interfaces.ICollisionObject;
import com.upm.pproject.models.gameobjects.drawable.interfaces.IDrawableObject;

import java.util.Set;

public interface IGameObjectManager {
    Player getPlayer();

    void setPlayer(Player player);

    void setBoxes(Set<Box> boxes);

    Set<Wall> getWalls();

    Set<Goal> getGoals();

    Set<Box> getBoxes();

    void newGameObject(int x, int y, GameObjectType type);

    Set<IDrawableObject> getDrawableObjects();

    Object getCollisionPlayer();

    Set<ICollisionObject> getCollisionWalls();

    Set<ICollisionObject> getCollisionBoxes();
}
