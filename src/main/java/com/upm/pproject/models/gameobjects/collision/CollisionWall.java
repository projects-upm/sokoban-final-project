package com.upm.pproject.models.gameobjects.collision;

import com.upm.pproject.models.gameobjects.Wall;

public class CollisionWall extends CollisionObject {
    /**
     * Constructor to set wall
     * @param wall
     */
    public CollisionWall(Wall wall) {
        this.gameObject = wall;
    }

}
