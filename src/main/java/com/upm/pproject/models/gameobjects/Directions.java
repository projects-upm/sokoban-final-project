package com.upm.pproject.models.gameobjects;

public enum Directions {
    UP, DOWN, LEFT, RIGHT
}
