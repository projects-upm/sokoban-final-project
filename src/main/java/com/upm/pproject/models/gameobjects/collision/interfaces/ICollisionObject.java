package com.upm.pproject.models.gameobjects.collision.interfaces;

import com.upm.pproject.models.gameobjects.Directions;
import com.upm.pproject.models.gameobjects.GameObject;

public interface ICollisionObject {
    GameObject getGameObject();

    boolean isCollision(ICollisionObject collisionObject, Directions direction);
}
