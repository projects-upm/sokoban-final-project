package com.upm.pproject.models.gameobjects;

import com.upm.pproject.models.gameobjects.collision.CollisionBox;
import com.upm.pproject.models.gameobjects.collision.CollisionPlayer;
import com.upm.pproject.models.gameobjects.collision.CollisionWall;
import com.upm.pproject.models.gameobjects.collision.interfaces.ICollisionObject;
import com.upm.pproject.models.gameobjects.drawable.DrawableBox;
import com.upm.pproject.models.gameobjects.drawable.DrawableGoal;
import com.upm.pproject.models.gameobjects.drawable.DrawablePlayer;
import com.upm.pproject.models.gameobjects.drawable.DrawableWall;
import com.upm.pproject.models.gameobjects.drawable.interfaces.IDrawableObject;
import com.upm.pproject.models.gameobjects.interfaces.IGameObjectManager;

import java.util.HashSet;
import java.util.Set;

public class GameObjectManager implements IGameObjectManager {

    private Player player;
    private Set<Wall> walls;
    private Set<Goal> goals;
    private Set<Box> boxes;

    private GameObjectFactory gameObjectFactory;

    /**
     * Constructor to set walls, goals and boxes.
     */
    public GameObjectManager() {
        this.walls = new HashSet<>();
        this.goals = new HashSet<>();
        this.boxes = new HashSet<>();
        this.gameObjectFactory = new GameObjectFactory();
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public void setPlayer(Player player) {
        this.player = new Player(player.getX(), player.getY());
    }

    @Override
    public ICollisionObject getCollisionPlayer() {
        return new CollisionPlayer(player);
    }

    @Override
    public Set<ICollisionObject> getCollisionBoxes() {
        Set<ICollisionObject> collisionObjects = new HashSet<>();
        for (Box box : this.boxes) {
            collisionObjects.add(new CollisionBox(box));
        }
        return collisionObjects;
    }

    @Override
    public Set<ICollisionObject> getCollisionWalls() {
        Set<ICollisionObject> collisionObjects = new HashSet<>();
        for (Wall wall : this.walls) {
            collisionObjects.add(new CollisionWall(wall));
        }
        return collisionObjects;
    }

    @Override
    public Set<Wall> getWalls() {
        return walls;
    }

    @Override
    public Set<Goal> getGoals() {
        return goals;
    }

    @Override
    public Set<Box> getBoxes() {
        return boxes;
    }

    @Override
    public void setBoxes(Set<Box> boxes) {
        this.boxes = new HashSet<>();
        for (Box box : boxes) {
            this.boxes.add(new Box(box.getX(), box.getY()));
        }
    }

    /**
     * Create a new GameObject in desired position
     * @param x
     * @param y
     * @param type
     */
    @Override
    public void newGameObject(int x, int y, GameObjectType type) {
        if (type == GameObjectType.PLAYER) {
            this.player = gameObjectFactory.createPlayer(x, y);
        } else if (type == GameObjectType.WALL) {
            this.walls.add(gameObjectFactory.createWall(x, y));
        } else if (type == GameObjectType.GOAL) {
            this.goals.add(gameObjectFactory.createGoal(x, y));
        } else {
            this.boxes.add(gameObjectFactory.createBox(x, y));
        }
    }

    @Override
    public Set<IDrawableObject> getDrawableObjects() {
        Set<IDrawableObject> drawableObjects = new HashSet<>();
        drawableObjects.add(new DrawablePlayer(this.player));
        for (Wall wall : this.walls) {
            drawableObjects.add(new DrawableWall(wall));
        }
        for (Goal goal : this.goals) {
            drawableObjects.add(new DrawableGoal(goal));
        }
        for (Box box : this.boxes) {
            drawableObjects.add(new DrawableBox(box));
        }
        return drawableObjects;
    }
}
