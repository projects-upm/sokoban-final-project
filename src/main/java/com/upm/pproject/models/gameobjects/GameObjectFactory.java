package com.upm.pproject.models.gameobjects;

import com.upm.pproject.models.gameobjects.interfaces.IGameObjectFactory;

public class GameObjectFactory implements IGameObjectFactory {
    /**
     * Create player in desired position
     * @param x
     * @param y
     * @return new Player
     */
    @Override
    public Player createPlayer(int x, int y) {
        return new Player(x, y);
    }

    /**
     * Create wall in disired position
     * @param x
     * @param y
     * @return new Wall
     */
    @Override
    public Wall createWall(int x, int y) {
        return new Wall(x, y);
    }

    /**
     * Create goal in desired position
     * @param x
     * @param y
     * @return new Goal
     */
    @Override
    public Goal createGoal(int x, int y) {
        return new Goal(x, y);
    }

    /**
     * Create box in desired position
     * @param x
     * @param y
     * @return new Box
     */
    @Override
    public Box createBox(int x, int y) {
        return new Box(x, y);
    }
}
