package com.upm.pproject.models.gameobjects.collision.interfaces;

public interface IMovableObject {

    void move(int deltaX, int deltaY);

}
