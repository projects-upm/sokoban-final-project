package com.upm.pproject.models.gameobjects;

public class Wall extends GameObject {
    /**
     * Dimensional Wall
     * @param x
     * @param y
     */
    public Wall(int x, int y) {
        super(x, y);
    }

}
