package com.upm.pproject.models.gameobjects.interfaces;

import com.upm.pproject.models.gameobjects.Box;
import com.upm.pproject.models.gameobjects.Goal;
import com.upm.pproject.models.gameobjects.Player;
import com.upm.pproject.models.gameobjects.Wall;

public interface IGameObjectFactory {
    Player createPlayer(int x, int y);

    Wall createWall(int x, int y);

    Goal createGoal(int x, int y);

    Box createBox(int x, int y);
}
