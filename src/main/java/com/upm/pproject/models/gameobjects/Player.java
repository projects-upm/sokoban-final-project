package com.upm.pproject.models.gameobjects;

public class Player extends GameObject {
    /**
     * Dimensional Player
     * @param x
     * @param y
     */
    public Player(int x, int y) {
        super(x, y);
    }
}
