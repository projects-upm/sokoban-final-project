package com.upm.pproject.models.gameobjects.drawable;

import com.upm.pproject.models.gameobjects.Box;
import com.upm.pproject.models.gameobjects.drawable.interfaces.IDrawableObject;

import java.awt.*;

public class DrawableBox implements IDrawableObject {

    private Box box;

    /**
     * Constructor to set box
     * @param box
     */
    public DrawableBox(Box box) {
        this.box = box;
    }

    /**
     * Draw Box
     * @param graphics
     */
    @Override
    public void draw(Graphics graphics) {
        graphics.setColor(Color.ORANGE);//Color

        final int leftUpperCornerX = box.getX() - box.getWidth() / 2;
        final int leftUpperCornerY = box.getY() - box.getHeight() / 2;

        graphics.drawRect(leftUpperCornerX, leftUpperCornerY, box.getWidth(), box.getHeight());//Draw box

        final int rightLowerCornerX = box.getX() + box.getWidth() / 2;
        final int rightLowerCornerY = box.getY() + box.getHeight() / 2;

        graphics.drawLine(leftUpperCornerX, leftUpperCornerY, rightLowerCornerX, rightLowerCornerY);//Draw box lines
        graphics.drawLine(rightLowerCornerX, leftUpperCornerY, leftUpperCornerX, rightLowerCornerY);//Draw box lines
    }

    /**
     * Draw offsets
     * @param graphics
     * @param xOffset
     * @param yOffset
     */
    @Override
    public void draw(Graphics graphics, int xOffset, int yOffset) {
        graphics.setColor(Color.ORANGE);//Color

        final int leftUpperCornerX = xOffset + (box.getX() - box.getWidth() / 2);
        final int leftUpperCornerY = yOffset + (box.getY() - box.getHeight() / 2);

        graphics.drawRect(leftUpperCornerX, leftUpperCornerY, box.getWidth(), box.getHeight());//Draw box

        final int rightLowerCornerX = xOffset + (box.getX() + box.getWidth() / 2);
        final int rightLowerCornerY = yOffset + (box.getY() + box.getHeight() / 2);

        graphics.drawLine(leftUpperCornerX, leftUpperCornerY, rightLowerCornerX, rightLowerCornerY);//Draw box lines
        graphics.drawLine(rightLowerCornerX, leftUpperCornerY, leftUpperCornerX, rightLowerCornerY);//Draw box lines
    }
}
