package com.upm.pproject.models.gameobjects.drawable;

import com.upm.pproject.models.gameobjects.Goal;
import com.upm.pproject.models.gameobjects.drawable.interfaces.IDrawableObject;

import java.awt.*;

public class DrawableGoal implements IDrawableObject {

    private Goal goal;

    /**
     * Constructor to set goal
     * @param goal
     */
    public DrawableGoal(Goal goal) {
        this.goal = goal;
    }

    /**
     * Draw Goal
     * @param graphics
     */
    @Override
    public void draw(Graphics graphics) {
        graphics.setColor(Color.GREEN);//Color of goal

        graphics.drawOval(goal.getX(), goal.getY(), goal.getWidth(), goal.getHeight());//Draw outline
        graphics.fillOval(goal.getX(), goal.getY(), goal.getWidth(), goal.getHeight());//Fill oval
    }

    /**
     * Draw offsets
     * @param graphics
     * @param xOffset
     * @param yOffset
     */
    @Override
    public void draw(Graphics graphics, int xOffset, int yOffset) {
        graphics.setColor(Color.GREEN);//Color of goal

        int x = xOffset + goal.getX();
        int y = yOffset + goal.getY();

        graphics.drawOval(x, y, goal.getWidth(), goal.getHeight());//Draw outline
        graphics.fillOval(x, y, goal.getWidth(), goal.getHeight());//Fill oval
    }
}
