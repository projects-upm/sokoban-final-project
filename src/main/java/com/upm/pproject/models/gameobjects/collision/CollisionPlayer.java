package com.upm.pproject.models.gameobjects.collision;

import com.upm.pproject.models.gameobjects.Player;
import com.upm.pproject.models.gameobjects.collision.interfaces.IMovableObject;

public class CollisionPlayer extends CollisionObject implements IMovableObject {
    /**
     * Constructor to set player
     * @param player
     */
    public CollisionPlayer(Player player) {
        this.gameObject = player;
    }

    /**
     * Move in the desired direction
     * @param deltaX
     * @param deltaY
     */
    @Override
    public void move(int deltaX, int deltaY) {
        gameObject.setPos(gameObject.getX() + deltaX, gameObject.getY() + deltaY);
    }

}
