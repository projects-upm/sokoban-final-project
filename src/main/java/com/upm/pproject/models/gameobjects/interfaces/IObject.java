package com.upm.pproject.models.gameobjects.interfaces;

public interface IObject {

    int getX();

    int getY();

    int getHeight();

    int getWidth();

    void setPos(int x, int y);

}
