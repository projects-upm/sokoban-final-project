package com.upm.pproject.models.gameobjects.drawable;

import com.upm.pproject.models.gameobjects.Wall;
import com.upm.pproject.models.gameobjects.drawable.interfaces.IDrawableObject;

import java.awt.*;

public class DrawableWall implements IDrawableObject {

    private Wall wall;

    /**
     * Constructor to set wall
     * @param wall
     */
    public DrawableWall(Wall wall) {
        this.wall = wall;
    }

    /**
     * Draw wall
     * @param graphics
     */
    @Override
    public void draw(Graphics graphics) {
        graphics.setColor(Color.GRAY);//Color of wall

        int leftUpperCornerX = wall.getX() - wall.getWidth() / 2;
        int leftUpperCornerY = wall.getY() - wall.getHeight() / 2;

        graphics.drawRect(leftUpperCornerX, leftUpperCornerY, wall.getWidth(), wall.getHeight());//Draw rectangle
        graphics.fillRect(leftUpperCornerX, leftUpperCornerY, wall.getWidth(), wall.getHeight());//Fill the rectangle
    }

    /**
     * Draw offsets
     * @param graphics
     * @param xOffset
     * @param yOffset
     */
    @Override
    public void draw(Graphics graphics, int xOffset, int yOffset) {
        graphics.setColor(Color.GRAY);//COlor of wall

        int leftUpperCornerX = xOffset + (wall.getX() - wall.getWidth() / 2);
        int leftUpperCornerY = yOffset + (wall.getY() - wall.getHeight() / 2);

        graphics.drawRect(leftUpperCornerX, leftUpperCornerY, wall.getWidth(), wall.getHeight());//Draw rectangle
        graphics.fillRect(leftUpperCornerX, leftUpperCornerY, wall.getWidth(), wall.getHeight());//Fill the rectangle
    }
}
