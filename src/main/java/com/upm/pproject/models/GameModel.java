package com.upm.pproject.models;

import com.upm.pproject.models.gameobjects.Box;
import com.upm.pproject.models.gameobjects.Directions;
import com.upm.pproject.models.gameobjects.Goal;
import com.upm.pproject.models.gameobjects.collision.CollisionPlayer;
import com.upm.pproject.models.gameobjects.collision.interfaces.ICollisionObject;
import com.upm.pproject.models.gameobjects.collision.interfaces.IMovableObject;
import com.upm.pproject.models.interfaces.IModel;
import com.upm.pproject.models.levels.LevelManager;
import com.upm.pproject.models.levels.LevelState;
import com.upm.pproject.models.levels.interfaces.ILevel;
import com.upm.pproject.models.levels.interfaces.ILevelManager;
import com.upm.pproject.models.levels.interfaces.ILevelState;
import com.upm.pproject.models.levels.interfaces.ISavedLevel;

import java.io.File;
import java.io.IOException;
import java.util.Deque;
import java.util.LinkedList;

public class GameModel implements IModel {

    public static final int FIELD_SELL_SIZE = 20;

    private ILevelManager loader;
    private int currentLevel;
    private ILevel level;
    private Deque<ILevelState> movements;
    private Deque<ILevelState> undoMovements;
    private int totalScore;

    /**
     * Default Constructor
     * @throws IOException
     */
    public GameModel() throws IOException {
        this.currentLevel = 1;
        this.loader = new LevelManager();
        this.restartLevel(this.currentLevel);
        this.movements = new LinkedList<>();
        this.undoMovements = new LinkedList<>();
        this.totalScore = 0;
    }

    /**
     * Constructor of savefiles
     * @param saveFile
     * @throws IOException
     */
    public GameModel(File saveFile) throws IOException {
        this.loader = new LevelManager();
        this.movements = new LinkedList<>();
        this.undoMovements = new LinkedList<>();
        ISavedLevel savedLevel = this.loader.loadSaveLevel(saveFile);
        this.level = savedLevel.getLevel();
        this.movements = savedLevel.getMovements();
        this.totalScore = savedLevel.getScore();
        this.currentLevel = this.level.getId();
    }

    /**
     * Restart level
     * @throws IOException
     */
    public void restart() throws IOException {
        this.restartLevel(currentLevel);
    }

    /**
     * Start next level
     * @return
     * @throws IOException
     */
    public boolean startNextLevel() throws IOException {
        this.totalScore += this.getLevelScore();
        return this.restartLevel(++currentLevel);
    }

    /**
     * Start new Game
     * @throws IOException
     */
    public void startNewGame() throws IOException {
        this.totalScore = 0;
        this.currentLevel = 1;
        this.restartLevel(currentLevel);
    }

    /**
     * Undo method
     * Pop the movement in the queue and set the position of player and boxes
     */
    public void undo() {
        if (!this.movements.isEmpty()) {
            this.undoMovements.push(new LevelState(this.level.getGameObjectManager().getPlayer(), this.level.getGameObjectManager().getBoxes()));
            ILevelState lastMovement = this.movements.pop();
            this.level.getGameObjectManager().setPlayer(lastMovement.getPlayer());
            this.level.getGameObjectManager().setBoxes(lastMovement.getBoxes());
        }
    }

    /**
     * Redo method
     * Pop the movement in the queue and set again the last position of player and boxes
     */
    public void redo() {
        if (!this.undoMovements.isEmpty()) {
            this.movements.push(new LevelState(this.level.getGameObjectManager().getPlayer(), this.level.getGameObjectManager().getBoxes()));
            ILevelState undoMovement = this.undoMovements.pop();
            this.level.getGameObjectManager().setPlayer(undoMovement.getPlayer());
            this.level.getGameObjectManager().setBoxes(undoMovement.getBoxes());
        }
    }

    /**
     * Save game in a file
     * @param saveFile
     * @throws IOException
     */
    public void saveGame(File saveFile) throws IOException {
        loader.saveLevelProgress(this.level, this.totalScore, this.movements, saveFile);
    }

    /**
     * Store the movements in the queue
     * @param direction
     */
    public void move(Directions direction) {
        // Save the prev state
        this.movements.push(new LevelState(this.level.getGameObjectManager().getPlayer(), this.level.getGameObjectManager().getBoxes()));
        // Reset the redo movements
        if (!this.undoMovements.isEmpty()) this.undoMovements = new LinkedList<>();
        // Check collisions and move objects
        final CollisionPlayer player = (CollisionPlayer) level.getGameObjectManager().getCollisionPlayer();
        if (checkWallCollision(player, direction) || checkBoxCollision(player, direction)) {
            this.movements.pop();
            return;
        }
        this.moveDirection(player, direction, GameModel.FIELD_SELL_SIZE);
    }

    public ILevel getCurrentLevel() {
        return this.level;
    }

    public String getLevelName() {
        return this.level.getName();
    }

    public int getTotalScore() {
        return this.totalScore;
    }

    public int getLevelScore() {
        return (this.level.getGameObjectManager().getBoxes().size() * 100) - this.movements.size();
    }

    /**
     * Restart level of given level
     * Load the new level
     * Creates new queues of movements (empty)
     * @param level
     * @return
     * @throws IOException
     */
    private boolean restartLevel(int level) throws IOException {
        this.level = this.loader.loadLevel(level);
        if (this.level == null) return false;
        this.movements = new LinkedList<>();
        this.undoMovements = new LinkedList<>();
        return true;
    }

    /**
     * Move the movable object to desired direction
     * @param movableObject
     * @param direction
     * @param value
     */
    private void moveDirection(IMovableObject movableObject, Directions direction, int value) {
        int deltaX = 0;
        int deltaY = 0;

        if (direction == Directions.LEFT) {
            deltaX = -value;
        } else if (direction == Directions.RIGHT) {
            deltaX = value;
        } else if (direction == Directions.UP) {
            deltaY = -value;
        } else {
            deltaY = value;
        }

        movableObject.move(deltaX, deltaY);
    }

    /**
     * Check if level is completed
     * @return
     */
    public boolean checkCompletion() {
        boolean completion = true;

        for (Goal home : level.getGameObjectManager().getGoals()) {
            boolean inPlace = false;
            for (Box box : level.getGameObjectManager().getBoxes()) {
                if (home.getX() == box.getX() && home.getY() == box.getY()) {
                    inPlace = true;
                    break;
                }
            }
            completion &= inPlace;
        }

        return completion;
    }

    /**
     * Check the wall collision
     * @param gameObject
     * @param direction
     * @return
     */
    private boolean checkWallCollision(ICollisionObject gameObject, Directions direction) {
        for (ICollisionObject object : level.getGameObjectManager().getCollisionWalls()) {
            if (gameObject.isCollision(object, direction)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check the box collision
     * @param collisionObject
     * @param direction
     * @return
     */
    private boolean checkBoxCollision(ICollisionObject collisionObject, Directions direction) {
        ICollisionObject collisionBox = null;

        for (ICollisionObject box : level.getGameObjectManager().getCollisionBoxes()) {
            if (collisionObject.isCollision(box, direction)) {
                collisionBox = box;
                if (this.checkWallCollision(box, direction)) {
                    return true;
                } else {
                    if (this.checkBoxBoxCollision(box, direction)) return true;
                    break;
                }
            }
        }

        if (collisionBox != null) {
            moveDirection((IMovableObject) collisionBox, direction, GameModel.FIELD_SELL_SIZE);
        }
        return false;
    }

    /**
     * Check if a box collide with another box
     * @param collisionObject
     * @param direction
     * @return
     */
    private boolean checkBoxBoxCollision(ICollisionObject collisionObject, Directions direction) {
        for (ICollisionObject b : level.getGameObjectManager().getCollisionBoxes()) {
            if (collisionObject.isCollision(b, direction)) {
                return true;
            }
        }
        return false;
    }
}
