package com.upm.pproject.models.levels.interfaces;

import com.upm.pproject.models.gameobjects.Box;
import com.upm.pproject.models.gameobjects.Player;

import java.util.Set;

public interface ILevelState {
    Player getPlayer();

    Set<Box> getBoxes();
}
