package com.upm.pproject.models.levels;

import com.upm.pproject.models.gameobjects.Box;
import com.upm.pproject.models.levels.interfaces.ILevel;
import com.upm.pproject.models.levels.interfaces.ILevelSave;
import com.upm.pproject.models.levels.interfaces.ILevelState;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Deque;

public class LevelSave implements ILevelSave {

    private FileWriter fileWriter;

    /**
     * Constructor to set file writer
     * @param saveFile
     * @throws IOException
     */
    LevelSave(File saveFile) throws IOException {
        fileWriter = new FileWriter(saveFile);
    }

    /**
     * Method to save status of the game in a file
     * Saves level, score and a list of movements
     * @param level
     * @param score
     * @param movements
     * @throws IOException
     */
    @Override
    public void save(ILevel level, int score, Deque<ILevelState> movements) throws IOException {
        fileWriter.write(level.getId() + "\n"); //Write in a file the level id
        fileWriter.write(score + "\n"); //Write the score
        fileWriter.write(level.getGameObjectManager().getPlayer().getX() + "-" + level.getGameObjectManager().getPlayer().getY() + "\n"); //Write player position
        for (Box box : level.getGameObjectManager().getBoxes()) {
            fileWriter.write(box.getX() + "-" + box.getY() + " ");//Write boxes distribution
        }
        fileWriter.write("\n");
        for (ILevelState levelState : movements) {
            fileWriter.write(levelState.getPlayer().getX() + "-" + levelState.getPlayer().getY() + "\n");//Write player movements
            for (Box box : levelState.getBoxes()) {
                fileWriter.write(box.getX() + "-" + box.getY() + " ");
            }
            fileWriter.write("\n");
        }
        fileWriter.close();
    }

}
