package com.upm.pproject.models.levels;

import com.upm.pproject.models.levels.interfaces.ILevel;
import com.upm.pproject.models.levels.interfaces.ILevelState;
import com.upm.pproject.models.levels.interfaces.ISavedLevel;

import java.util.Deque;

public class SavedLevel implements ISavedLevel {
    private ILevel level;
    private int score;
    private Deque<ILevelState> movements;

    /**
     * Constructor to set saved level
     * @param level
     * @param score
     * @param movements
     */
    SavedLevel(ILevel level, int score, Deque<ILevelState> movements) {
        this.level = level;
        this.score = score;
        this.movements = movements;
    }

    @Override
    public ILevel getLevel() {
        return level;
    }

    @Override
    public int getScore() {
        return score;
    }

    @Override
    public Deque<ILevelState> getMovements() {
        return movements;
    }
}
