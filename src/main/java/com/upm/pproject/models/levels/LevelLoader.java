package com.upm.pproject.models.levels;

import com.upm.pproject.models.GameModel;
import com.upm.pproject.models.gameobjects.GameObjectType;
import com.upm.pproject.models.levels.interfaces.ILevel;
import com.upm.pproject.models.levels.interfaces.ILevelLoader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class LevelLoader implements ILevelLoader {

    private static final Logger logger = LogManager.getLogger(LevelLoader.class);

    private BufferedReader br;
    private ILevel level;

    /**
     * Load level of given file
     * @param levelFile
     * @throws IOException
     */
    public LevelLoader(File levelFile) throws IOException {
        if (levelFile != null) {//File not empty
            br = new BufferedReader(new FileReader(levelFile));
            String load = "Preparing to load level " + levelFile.getName();
            logger.debug(load);
            this.preload(levelFile);
            String success = "Success load level " + levelFile.getName();
            logger.debug(success);
        }
    }

    @Override
    public ILevel load() {
        return level;
    }

    /**
     * Preload level of given file
     * @param file
     * @throws IOException
     */
    private void preload(File file) throws IOException {
        int id = Integer.parseInt(file.getName().replaceFirst("[.][^.]+$", "").split("_")[1]); //Set id with id in the file
        this.createLevel(id);
        this.loadMaze();//Load map
    }

    /**
     * Create level of given id
     * @param id
     * @throws IOException
     */
    private void createLevel(int id) throws IOException {
        String name = br.readLine();
        String[] dim = br.readLine().split(" ");
        int dimX = Integer.parseInt(dim[0]);//Dimension x
        int dimY = Integer.parseInt(dim[1]);//Dimension y
        this.level = new Level(id, name, dimX, dimY);//Creates level
    }

    /**
     * Load the map of file
     * # Box
     * * Goal
     * W Player
     * + Wall
     * @throws IOException
     */
    private void loadMaze() throws IOException {
        int x = GameModel.FIELD_SELL_SIZE / 2;
        int y = GameModel.FIELD_SELL_SIZE / 2;
        String line = br.readLine();
        while (line != null) {
            for (int i = 0; i < line.length(); i++) {
                char car = line.charAt(i);
                if (car == '#') {
                    level.getGameObjectManager().newGameObject(x, y, GameObjectType.BOX);
                } else if (car == '*') {
                    level.getGameObjectManager().newGameObject(x, y, GameObjectType.GOAL);
                } else if (car == 'W') {
                    level.getGameObjectManager().newGameObject(x, y, GameObjectType.PLAYER);
                } else if (car == '+') {
                    level.getGameObjectManager().newGameObject(x, y, GameObjectType.WALL);
                }
                x += GameModel.FIELD_SELL_SIZE;
            }
            line = br.readLine();
            x = GameModel.FIELD_SELL_SIZE / 2;
            y += GameModel.FIELD_SELL_SIZE;
        }
    }
}
