package com.upm.pproject.models.levels.interfaces;

public interface ILevelLoader {
    ILevel load();
}
