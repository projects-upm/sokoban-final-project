package com.upm.pproject.models.levels;

import com.upm.pproject.models.gameobjects.Box;
import com.upm.pproject.models.gameobjects.Player;
import com.upm.pproject.models.levels.interfaces.ILevelState;

import java.util.HashSet;
import java.util.Set;

public class LevelState implements ILevelState {
    private Player player;
    private Set<Box> boxes;

    /**
     * Constructor to set LevelState
     * @param player
     * @param boxes
     */
    public LevelState(Player player, Set<Box> boxes) {
        this.player = new Player(player.getX(), player.getY());
        this.boxes = new HashSet<>();
        for (Box box : boxes) {
            this.boxes.add(new Box(box.getX(), box.getY()));
        }
    }

    public Player getPlayer() {
        return player;
    }

    public Set<Box> getBoxes() {
        return boxes;
    }
}
