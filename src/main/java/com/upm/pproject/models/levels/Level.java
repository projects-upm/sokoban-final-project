package com.upm.pproject.models.levels;

import com.upm.pproject.models.gameobjects.GameObjectManager;
import com.upm.pproject.models.gameobjects.drawable.interfaces.IDrawableObject;
import com.upm.pproject.models.gameobjects.interfaces.IGameObjectManager;
import com.upm.pproject.models.levels.interfaces.ILevel;

import java.util.Set;

public class Level implements ILevel {

    private IGameObjectManager gameObjectManager;
    private int id;
    private int dimX;
    private int dimY;
    private String name;

    /**
     * Constructor to set the level
     * @param id
     * @param name
     * @param dimX
     * @param dimY
     */
    Level(int id, String name, int dimX, int dimY) {
        this.id = id;
        this.name = name;
        this.dimX = dimX;
        this.dimY = dimY;
        this.gameObjectManager = new GameObjectManager();
    }

    @Override
    public IGameObjectManager getGameObjectManager() {
        return this.gameObjectManager;
    }

    @Override
    public Set<IDrawableObject> getDrawableObjects() {
        return gameObjectManager.getDrawableObjects();
    }

    @Override
    public int getDimX() {
        return dimX;
    }

    @Override
    public int getDimY() {
        return dimY;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getId() {
        return id;
    }
}
