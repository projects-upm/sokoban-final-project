package com.upm.pproject.models.levels.interfaces;

import java.io.File;
import java.io.IOException;
import java.util.Deque;

public interface ILevelManager {
    ILevel loadLevel(int n) throws IOException;

    void saveLevelProgress(ILevel level, int score, Deque<ILevelState> movements, File saveFile) throws IOException;

    ISavedLevel loadSaveLevel(File saveFile) throws IOException;
}
