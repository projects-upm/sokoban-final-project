package com.upm.pproject.models.levels;

import com.upm.pproject.models.levels.interfaces.ILevel;
import com.upm.pproject.models.levels.interfaces.ILevelManager;
import com.upm.pproject.models.levels.interfaces.ILevelState;
import com.upm.pproject.models.levels.interfaces.ISavedLevel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class LevelManager implements ILevelManager {

    private static final Logger logger = LogManager.getLogger(LevelManager.class);

    private Map<Integer, File> levels;

    /**
     * Create HashMap of levels
     * Set Path to levels
     */
    public LevelManager() {
        this.levels = new HashMap<>();
        initLevels("levels/");
    }

    /**
     * Read the files of levels in the given path and put the levels found in the HashMap
     * @param path
     */
    public void initLevels(String path) {
        File fileName = new File(Objects.requireNonNull(getClass().getClassLoader().getResource(path)).getPath());
        File[] fileList = fileName.listFiles(); //list files

        for (File file : Objects.requireNonNull(fileList)) {
            String load = "Discovered Level: " + file.getName();
            logger.debug(load);

            int id = Integer.parseInt(file.getName().replaceFirst("[.][^.]+$", "").split("_")[1]); //Set id with id in the file
            levels.put(id, file);//Put the id and file in HashMap levels
        }
    }

    /**
     * Load desired level
     * @param n
     * @return
     * @throws IOException
     */
    @Override
    public ILevel loadLevel(int n) throws IOException {
        return new LevelLoader(levels.get(n)).load();
    }

    /**
     * Save current progress in a file
     * @param level
     * @param score
     * @param movements
     * @param saveFile
     * @throws IOException
     */
    @Override
    public void saveLevelProgress(ILevel level, int score, Deque<ILevelState> movements, File saveFile) throws IOException {
        new LevelSave(saveFile).save(level, score, movements);
    }

    /**
     * Load saved level in a file
     * @param saveFile
     * @return
     * @throws IOException
     */
    @Override
    public ISavedLevel loadSaveLevel(File saveFile) throws IOException {
        return new SaveLoader(saveFile).load(this.levels);
    }

}
