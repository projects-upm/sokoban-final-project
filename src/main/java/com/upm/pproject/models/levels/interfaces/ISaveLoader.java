package com.upm.pproject.models.levels.interfaces;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public interface ISaveLoader {
    ISavedLevel load(Map<Integer, File> levels) throws IOException;
}
