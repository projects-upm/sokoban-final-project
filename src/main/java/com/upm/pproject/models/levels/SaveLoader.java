package com.upm.pproject.models.levels;

import com.upm.pproject.models.gameobjects.Box;
import com.upm.pproject.models.gameobjects.Player;
import com.upm.pproject.models.levels.interfaces.ILevel;
import com.upm.pproject.models.levels.interfaces.ILevelState;
import com.upm.pproject.models.levels.interfaces.ISaveLoader;
import com.upm.pproject.models.levels.interfaces.ISavedLevel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class SaveLoader implements ISaveLoader {

    private BufferedReader br;
    private ILevel level;

    /**
     * Constructor to load saves files
     * @param saveFile
     * @throws IOException
     */
    SaveLoader(File saveFile) throws IOException {
        this.br = new BufferedReader(new FileReader(saveFile));
    }

    /**
     * Load saves files
     * @param levels
     * @return
     * @throws IOException
     */
    public ISavedLevel load(Map<Integer, File> levels) throws IOException {
        // Get Level
        level = this.getLevel(this.readId(), levels);
        // Score
        int score = this.readScore();
        // Get save movements
        Deque<ILevelState> movements = this.readMovements();
        // Get the last state of the game
        if (!movements.isEmpty()) {
            ILevelState lastState = movements.pop();
            level.getGameObjectManager().setPlayer(lastState.getPlayer());
            level.getGameObjectManager().setBoxes(lastState.getBoxes());
        }
        return new SavedLevel(level, score, movements);
    }

    /**
     * Read the id in a file
     * @return
     * @throws IOException
     */
    private int readId() throws IOException {
        return Integer.parseInt(br.readLine());
    }

    /**
     * Read the score in a file
     * @return
     * @throws IOException
     */
    private int readScore() throws IOException {
        return Integer.parseInt(br.readLine());
    }

    private ILevel getLevel(int id, Map<Integer, File> levels) throws IOException {
        return new LevelLoader(levels.get(id)).load();
    }

    /**
     * Read player position
     * @param playerPos
     * @return Player
     * @throws IOException
     */
    private Player readPlayer(String playerPos) throws IOException {
        String[] sPlayer = playerPos.split("-");
        if (sPlayer.length != 2) {
            throw new IOException("SAVE ERROR: Error in player format");
        }
        int xPlayer = Integer.parseInt(sPlayer[0]);//Xpos
        int yPlayer = Integer.parseInt(sPlayer[1]);//Ypos
        return new Player(xPlayer, yPlayer);
    }

    /**
     * Read Boxes position in a file
     * @param level
     * @return HashMap of boxes
     * @throws IOException
     */
    private Set<Box> readBoxes(ILevel level) throws IOException {
        String[] boxesPos = br.readLine().split(" ");
        Set<Box> boxes = new HashSet<>();
        if (boxesPos.length != level.getGameObjectManager().getBoxes().size()) {
            throw new IOException("SAVE ERROR: Saved Wrong number of boxes");
        }
        for (String boxPos : boxesPos) {
            boxes.add(this.readBox(boxPos));
        }
        return boxes;
    }

    /**
     * Read Box position
     * @param boxPos
     * @return new Box(x,y)
     * @throws IOException
     */
    private Box readBox(String boxPos) throws IOException {
        String[] sBox = boxPos.split("-");
        if (sBox.length != 2) {
            throw new IOException("SAVE ERROR: Error in Box format.");
        }
        return new Box(Integer.parseInt(sBox[0]), Integer.parseInt(sBox[1]));
    }

    /**
     * Read the list of movements and set it to a LinkedList
     * @return Linkedlist of a movements
     * @throws IOException
     */
    private Deque<ILevelState> readMovements() throws IOException {
        Deque<ILevelState> movements = new LinkedList<>();
        String playerPos;
        while ((playerPos = br.readLine()) != null) {
            // Get save player position
            Player player = this.readPlayer(playerPos);

            // Get save boxes position
            Set<Box> boxes = this.readBoxes(level);

            movements.add(new LevelState(player, boxes));
        }
        return movements;
    }
}
