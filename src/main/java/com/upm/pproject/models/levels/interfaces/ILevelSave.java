package com.upm.pproject.models.levels.interfaces;

import java.io.IOException;
import java.util.Deque;

public interface ILevelSave {
    void save(ILevel level, int score, Deque<ILevelState> movements) throws IOException;
}
