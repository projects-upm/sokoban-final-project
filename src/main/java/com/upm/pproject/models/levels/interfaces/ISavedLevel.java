package com.upm.pproject.models.levels.interfaces;

import java.util.Deque;

public interface ISavedLevel {
    ILevel getLevel();

    int getScore();

    Deque<ILevelState> getMovements();
}
