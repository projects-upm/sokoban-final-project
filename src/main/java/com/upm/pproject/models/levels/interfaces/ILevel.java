package com.upm.pproject.models.levels.interfaces;

import com.upm.pproject.models.gameobjects.drawable.interfaces.IDrawableObject;
import com.upm.pproject.models.gameobjects.interfaces.IGameObjectManager;

import java.util.Set;

public interface ILevel {
    IGameObjectManager getGameObjectManager();

    Set<IDrawableObject> getDrawableObjects();

    int getDimX();

    int getDimY();

    String getName();

    int getId();
}
